# Code to rearrange historical swap and cap data for all regions from ICAP market data into SQL market data report. 
# Export output to csv for loading directly to SQL table.
# Same files as loaded and tested in Dev environment. Applicable for ICAP data.
# This code does not need to be run when loading into production. Used for initial aggregation of historical data.


#install.packages("odbc")     # <--- !! this one, not RODBC or RSQLServer !!
#install.packages("DBI")
#install.packages("data.table")
#install.packages("readxl")
#install.packages("lubridate")
#install.packages("openxlsx")


# Part of package to load historical broker quotes into production database within CleanCo Environment.
# This file does not need to be run again.
# Loading of historical data to be complete by Aaron Smith. Daily process can be run by anyone.
# Author: Aaron Smith

library(data.table)
library(odbc)
library(DBI)
library(readxl)
library(lubridate)
library(openxlsx)


#error log creation
error_log <- file(paste("C:/Users/Aaron.Smith/OneDrive - CleanCo ",
                        "Queensland Limited/Documents/R/Projects/Market Data ",
                        "Database/error_log.txt", sep = ""), open = "a")

# Connect to the database
#  - use the username stored in the user's environment
#  - watch for a pop up window the first time
warn_error_database <- tryCatch({
  server = "cql-dev.database.windows.net"
  database = "tradingrisk"
  con = DBI::dbConnect(odbc::odbc(),
                       UID = 'tradingrisk', PWD = '@@12_lifted_WINGS_keep_53@@',
                       Driver="ODBC Driver 17 for SQL Server",
                       Server = server, Database = database)
  
},
error = function(e) {
  
  message("Error connecting to database, check connection settings")
  message(e)
  write(paste(Sys.time(), "Error connecting to database, check connection settings\n",
              sep = "\t"), error_log, append = TRUE)
  
  write(paste(Sys.time(), e, "\n", sep = "\t"), error_log, append = TRUE)
})


### Read market_data file 
#### obtain term and start date/end date data
we_read_icap <- tryCatch({
  market_data <- read.csv(paste("C:/Users/Aaron.Smith/OneDrive - CleanCo ",
                                "Queensland Limited/Documents/R/Projects/",
                                "Market Data Database/sql data/market_data.csv",  sep = ""), 
                          header = FALSE, stringsAsFactors = FALSE)
  
  colnames(market_data) <- c("publication_date", "broker", "subcommodity",
                             "instrument", "price_type", "profile", "term",
                             "boqtrflag", "bofyflag", "bocalyflag", "start_date", "end_date", "price", "premium") 
  market_data$term <- as.character(market_data$term)
  market_data$publication_date <- as.Date(market_data$publication_date, origin = "1970-01-01")
  market_data$start_date <- as.Date(market_data$start_date, origin = "1970-01-01")
  market_data$end_date <- as.Date(market_data$end_date, origin = "1970-01-01") 
  

  
},
error = function(e) {
  message("Error reading generation data, check format/filepath")
  message(e)
  write(paste(Sys.time(), "Error reading term and dates, check format/filepath",
              "\n", sep = "\t"), append = TRUE, error_log)
  write(paste(Sys.time(), e, "\n", sep = "\t"), append = TRUE, error_log)
},
warning = function(w){
  write(paste(Sys.time(), w, "\n", sep = "\t"), append = TRUE, error_log)  
}) 






#push data to database
#add instrument and term information. Pull current term information, 
#check if contains, if not, push to db, if yes, flag yes and add price data to table

sql_term <- dbGetQuery(con, sqlInterpolate(con, "select trim(Name) as term, StartDate as start_date, EndDate as end_date from MarketData.Term order by id"))
sql_instrument <- dbGetQuery(con,sqlInterpolate(con, "select trim(Name) as instrument, trim(subcommodity) as subcommodity, trim(subcommodityfull) as subcommodityfull, trim(profile) as profile, trim(producttype) as producttype from MarketData.Instruments order by id"))

u_term <- unique(market_data[c("term", "start_date", "end_date")])
u_term$term <- as.character(u_term$term)


#check if any unique_term_stc or unique_term_lgc are new compared to sql_term/sql_instrument

# #
# if(nrow(intersect(u_term[,], sql_term[,])) != nrow(u_term)){
#   message("New LGC instrument Term in data - Not added to database")
#   stop("New LGC instrument Term in data - Not added to database")
# }

#####Prices table to be populated with foreign keys referencing primary key from 
# Instrument and Term tables. 
# In place to restrict price table to known constraints on "instrument", "subcommodity",
# "profile", "term"

#Prices column names
#colnames -> PublicationDate = publication_date
#            InstrumentID = (primary key from Instrument table)
#            TermID = (primary key from Term table)
#            Price_Type = bid/ask/settle
#            Price = price
#            Premium = premium
#            Broker = provider
#            ModifiedDate = sysdatetime()
# 


#            Primary KEY (ID),
#            FOREIGN KEY (InstrumentID) REFERENCES MarketData.Instruments(ID), --Used to keep track of instrument data being upload to prices. Can easily check inputs against Instruments
#            FOREIGN KEY (TermID) REFERENCES MarketData.Term(ID) --Used to keep track of term data



# 
# Run code from here to populate data table to excel for storage 
price_table <- data.table(
  PublicationDate = character(),
  InstrumentID = numeric(),
  TermID = numeric(),
  BoQtrFlag = numeric(),
  BoFYFlag = numeric(),
  BoCALYFlag = numeric(),
  PriceType = character(),
  Price = numeric(),
  Premium = numeric(),
  Broker = character(),
  ModifiedDate = character()
)

# assign term_id and instrument_id based on foreign keys
# bind to price_table and export. 
# if needing to run again, change name of file to output to.
for (i in 1:nrow(market_data)){
  term_id <- which(sql_term$term == market_data$term[i] & sql_term$start_date == market_data$start_date[i])
  instrument_id <- which(sql_instrument$instrument == market_data$instrument[i] &
                           sql_instrument$subcommodity == toupper(market_data$subcommodity[i]) &
                           sql_instrument$profile == market_data$profile[i])
  if(is.null(term_id) || is.null(instrument_id)) {
    write(paste(Sys.time(), pub_date, "term_id or instrument id NULL", sep = "\t"), append = TRUE, error_log)
  }else {
    price_table_row <-list(as.character(market_data$publication_date[i]), instrument_id, term_id, market_data$boqtrflag[i], market_data$bofyflag[i], market_data$bocalyflag[i],
                           market_data$price_type[i], market_data$price[i], market_data$premium[i], market_data$broker[i], as.character(Sys.Date()))
    
    price_table <- rbind(price_table, price_table_row)
  }

}

price_table$PriceType[which(price_table$PriceType == "Sett")] <- "SETTLE"
price_table$PriceType <- toupper(price_table$PriceType)

#write to excel
path <- paste("C:/Users/Aaron.Smith/OneDrive - CleanCo ",
              "Queensland Limited/Documents/R/Projects/",
              "Market Data Database/sql_market_data_new_1.csv",  sep = "")

write.csv(price_table, file = path, sep = ',', append = FALSE, row.names = FALSE, col.names = FALSE)
# 





